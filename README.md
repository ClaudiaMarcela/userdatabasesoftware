# UserDatabaseSoftware

_Software to automate the process of creating, consulting, updating and deleting users in a database._



## Developed with 🛠️

_This sofware was developed with Python with an object-oriented programming approach, with Data Access Object (DAO) pattern. Data were storaged in PostgreSQL, and queries were executed by a connection pool. The following tools were used:_
* PyCharm - IDE for code writing
* pgAdmin 4 - PostgreSQL database manager



## Executing 🚀

_These screenshots display the functionalities of the software:_

* Register a new user on database

![Screenshot](DOCUMENTATION/img/Create user.png)

* See all users registered on database

![Screenshot](DOCUMENTATION/img/Read user.png)

* Update information about an user

![Screenshot](DOCUMENTATION/img/Update user.png)

* Delete a user from database

![Screenshot](DOCUMENTATION/img/Delete user.png)



## Project date 📌

2021
